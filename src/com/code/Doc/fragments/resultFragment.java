package com.code.Doc.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.code.Doc.Busy;
import com.code.Doc.Details;
import com.code.Doc.Main;
import com.code.Doc.R;
import com.code.Doc.engine.Data;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class resultFragment extends Fragment{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View view =  inflater.inflate(R.layout.fragment_results, container, false);
		 Context context = getActivity().getApplicationContext();
		 
		 TextView v = (TextView)view.findViewById(R.id.results);
		 TextView p = (TextView)view.findViewById(R.id.probable);
		 TextView p2 = (TextView)view.findViewById(R.id.probable2);
		 TextView p3 = (TextView)view.findViewById(R.id.probable3);
		 ImageButton b = (ImageButton)view.findViewById(R.id.tomain);
		 v.setText(Data.classifiedResult);
		 
		 String[] data = Data.rank[0].split(",");
		 p.setText(data[0]+"     "+data[1].substring(0,4)+"%");
		 data = Data.rank[1].split(",");
		 p2.setText(data[0]+"     "+data[1].substring(0,4)+"%");
		 data = Data.rank[2].split(",");
		 p3.setText(data[0]+"     "+data[1].substring(0,4)+"%");
		 
		 
		 
		 b.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	Data.selectedDisease = -1;
	            	getActivity().finish();
	            	Intent i = new Intent(getActivity(),Main.class);
	    			resultFragment.this.startActivity(i);
	            	
	            }
	        });
		 
		 
		 v.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	Data.selectedDisease = Data.getDiseaseIndex(Data.classifiedResult);
	            	
	            	Intent i = new Intent(getActivity(),Busy.class);
	    			resultFragment.this.startActivity(i);
	            	
	            }
	        });
		 
		 p.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	Data.selectedDisease = Data.getDiseaseIndex(Data.rank[0].split(",")[0]);
	            
	            	Intent i = new Intent(getActivity(),Busy.class);
	    			resultFragment.this.startActivity(i);
	            	
	            }
	        });
		 
		 p2.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	Data.selectedDisease = Data.getDiseaseIndex(Data.rank[1].split(",")[0]);
	            	
	            	Intent i = new Intent(getActivity(),Busy.class);
	    			resultFragment.this.startActivity(i);
	            }
	        });
		 
		 p3.setOnClickListener(new OnClickListener() {
	            @Override
	            public void onClick(View v) {
	            	Data.selectedDisease = Data.getDiseaseIndex(Data.rank[2].split(",")[0]);
	            	Intent i = new Intent(getActivity(),Busy.class);
	    			resultFragment.this.startActivity(i);
	            	
	            }
	        });
		 return view;
		 }
	

	
}