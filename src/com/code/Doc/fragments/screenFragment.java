package com.code.Doc.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.code.Doc.Details;
import com.code.Doc.R;
import com.code.Doc.Results;
import com.code.Doc.Screen;
import com.code.Doc.engine.Data;
import com.code.Doc.engine.EngineVars;
import com.code.Doc.views.Group;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class screenFragment extends Fragment{
	private View view;
	private Context context;
	private Button button;
	
	SparseArray<Group> groups = new SparseArray<Group>();
	
	private void getdata(){
		String[] group = Data.getCategories();
		for(int i=0;i<group.length;i++){
			Group g = new Group(group[i]);
			String[] childrens = Data.organisedList.get(group[i]);
			for(int j=0;j<childrens.length;j++){
				g.children.add(childrens[j]);
			}
			groups.append(i,g);
		}
	}
	
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		getActivity().finish();
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		 getdata();
		ExpandableListView listView = (ExpandableListView)getActivity().findViewById(R.id.listView);
	    MyExpandableListAdapter adapter = new MyExpandableListAdapter(getActivity(),groups);
	    listView.setAdapter(adapter);
	    
	    button= (Button) getActivity().findViewById(R.id.done);
	    button.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	if(Data.selected.size()<5)
	        		Toast.makeText(getActivity().getApplicationContext(),"Insufficient selection, Please select more", Toast.LENGTH_SHORT).show();
	        	else{
	        		//prepare the data
	        		String data = new String("(");
	        		for(int i=0;i<Data.selected.size()-1;i++){
	        			data += String.valueOf(Data.selected.get(i))+",";
	        		}
	        		data += String.valueOf(Data.selected.get(Data.selected.size()-1))+")";
	     
	        		eng en = new eng();
	        		en.execute(data);
	        		screenFragment.this.button.setEnabled(false);
	        	}
	        	
	      
	        }
	    });

	}

	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,                   Bundle              savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_screen, null);
    	return v;
	}
	
	public class MyExpandableListAdapter extends BaseExpandableListAdapter {

		  private final SparseArray<Group> groups;
		  public LayoutInflater inflater;
		  public Activity activity;

		  public MyExpandableListAdapter(Activity act, SparseArray<Group> groups) {
		    activity = act;
		    this.groups = groups;
		    inflater = act.getLayoutInflater();
		  }

		  @Override
		  public Object getChild(int groupPosition, int childPosition) {
		    return groups.get(groupPosition).children.get(childPosition);
		  }

		  @Override
		  public long getChildId(int groupPosition, int childPosition) {
		    return 0;
		  }

		  @Override
		  public View getChildView(int groupPosition, final int childPosition,
		      boolean isLastChild, View convertView, ViewGroup parent) {
		    final String children = (String) getChild(groupPosition, childPosition);
		    TextView text = null;
		    if (convertView == null) {
		      convertView = inflater.inflate(R.layout.listrow_details, null);
		    }
		    text = (TextView) convertView.findViewById(R.id.textView1);
		    text.setText(children);
		    convertView.setOnClickListener(new OnClickListener() {
		      @Override
		      public void onClick(View v) {
		        Toast.makeText(activity, children,
		            Toast.LENGTH_SHORT).show();
		      }
		    });
		    
		    CheckBox box = (CheckBox) convertView.findViewById(R.id.check);
		    box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

	            @Override
	            public void onCheckedChanged(CompoundButton buttonView,
	                boolean isChecked) {
	            	if(isChecked){
	            		Data.deleteValIfExists(Data.getSelectedSymptomID(children));
	            		Data.selected.add(Data.getSelectedSymptomID(children));
	            	}else{
	            		Data.deleteValIfExists(Data.getSelectedSymptomID(children));
	            	}
	            }
	          });
		    
		    
		    
		    return convertView;
		  }

		  @Override
		  public int getChildrenCount(int groupPosition) {
		    return groups.get(groupPosition).children.size();
		  }

		  @Override
		  public Object getGroup(int groupPosition) {
		    return groups.get(groupPosition);
		  }

		  @Override
		  public int getGroupCount() {
		    return groups.size();
		  }

		  @Override
		  public void onGroupCollapsed(int groupPosition) {
		    super.onGroupCollapsed(groupPosition);
		  }

		  @Override
		  public void onGroupExpanded(int groupPosition) {
		    super.onGroupExpanded(groupPosition);
		  }

		  @Override
		  public long getGroupId(int groupPosition) {
		    return 0;
		  }

		  @Override
		  public View getGroupView(int groupPosition, boolean isExpanded,
		      View convertView, ViewGroup parent) {
		    if (convertView == null) {
		      convertView = inflater.inflate(R.layout.listrow_group, null);
		    }
		    Group group = (Group) getGroup(groupPosition);
		    TextView v = (TextView)convertView.findViewById(R.id.textView1);
		    v.setText(group.string);
		    
		    return convertView;
		  }

		  @Override
		  public boolean hasStableIds() {
		    return false;
		  }

		  @Override
		  public boolean isChildSelectable(int groupPosition, int childPosition) {
		    return false;
		  }
		} 
	
		
	public class eng extends AsyncTask<String, Integer, Void>  {
		
		private View view;
		private ProgressDialog dialog;
		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			
		}

		

		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
			String data = ((String[])params)[0];
			classifyData(data);
			rankData(data);
			return null;
		}



		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			
			
		}

		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		protected void onPostExecute(Void arg) {
			// TODO Auto-generated method stub
			super.onPostExecute(arg);
			//Toast.makeText(getActivity().getApplicationContext(), Data.classifiedResult, Toast.LENGTH_LONG).show();
			Intent i = new Intent(getActivity(),Results.class);
			screenFragment.this.startActivity(i);
			
//			Intent i = new Intent(getActivity(),Details.class);
//			screenFragment.this.startActivity(i);
			//EngineVars.caller.onDestroy();
		}

		private String classifyData(String data){
			String engineUrl = "http://myguide.pythonanywhere.com/classify/"+data+'/';
			InputStream is = null;
			String result = "";
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpGet req = new HttpGet(engineUrl);
				HttpResponse response = httpclient.execute(req);
				is = response.getEntity().getContent();
				result = convertInputStreamToString(is);
				Data.classifiedResult = result;
			}catch(Exception e){
				
				Log.e("trace", "err",e);
				result = "exception";
			}
			return result;
		}
		
		private String rankData(String data){
			String engineUrl = "http://myguide.pythonanywhere.com/rank/"+data+'/';
			InputStream is = null;
			String result = "";
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpGet req = new HttpGet(engineUrl);
				HttpResponse response = httpclient.execute(req);
				is = response.getEntity().getContent();
				result = convertInputStreamToString(is);
				result = result.substring(1, result.length()-1);
				Data.rank = result.split("&");
			}catch(Exception e){
				
				Log.e("trace", "err",e);
				result = "exception";
			}
			return result;
		}
		
		private  String convertInputStreamToString(InputStream inputStream) throws IOException{
	        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	        String line = "";
	        String result = "";
	        while((line = bufferedReader.readLine()) != null)
	            result += line;
	 
	        inputStream.close();
	        return result;
	 
	    }
		
		public boolean isConnected(Context context){
	        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
	            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	            if (networkInfo != null && networkInfo.isConnected())
	                return true;
	            else
	                return false;  
	    }
	}

		 
}