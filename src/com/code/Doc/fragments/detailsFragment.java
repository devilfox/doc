package com.code.Doc.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.code.Doc.R;
import com.code.Doc.engine.Data;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class detailsFragment extends Fragment{
	private View view;
	private Context context;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View vw = inflater.inflate(R.layout.fragment_details, container, false);
		 view = vw;
		 context = getActivity().getApplicationContext();
		 
	//	eng e = new eng();
		//e.execute(String.valueOf(Data.selectedDisease));
		 
		ImageButton next = (ImageButton)vw.findViewById(R.id.next);
		ImageButton prev = (ImageButton)vw.findViewById(R.id.prev);
		ImageButton back = (ImageButton)vw.findViewById(R.id.back);
		final Button push = (Button)vw.findViewById(R.id.push);
		
		final TextView heading = (TextView)vw.findViewById(R.id.heading);
		final TextView content = (TextView)vw.findViewById(R.id.content);
		
		heading.setText(getHeading(0));
		content.setText(getContent(0));
		
		
		next.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
	            //Do stuff here
		    	Data.currentpage++;
		    	if(Data.currentpage>=0&&Data.currentpage<=5){
		    		heading.setText(getHeading(Data.currentpage));
		    		content.setText(getContent(Data.currentpage));
		    	}
		    	else{
		    		Data.currentpage = 5;
		    	}
	    }
	});
		
		prev.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
	            //Do stuff here
		    	Data.currentpage--;
		    	if(Data.currentpage>=0&&Data.currentpage<=5){
		    		heading.setText(getHeading(Data.currentpage));
		    		content.setText(getContent(Data.currentpage));
		    	}
		    	else{
		    		Data.currentpage = 0;
		    	}
		    	
	    }
	});
		
		back.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
	            //Do stuff here
		    	getActivity().finish();
	    }
	});
		
		push.setOnClickListener(new Button.OnClickListener() {
		    public void onClick(View v) {
	            //Do stuff here
		    	push p = new push();
		    	p.execute();
		    	push.setEnabled(false);
	    }
	});
	
		
		 return view;
	}
	
private String getHeading(int page){
	switch (page){
	case 0:
		return "Introduction";
	case 1:
		return "Cause";
		
	case 2:
		return "Symptoms";
	case 3:
		return "Prevention";
	case 4:
		return "Treatment";
	case 5:
		return "When to see doctor";
	}
	return "";
}

private String getContent(int page){
	switch (page){
	case 0:
		return Data.Intro;
	case 1:
		return Data.Cause;
		
	case 2:
		return Data.Symptoms;
	case 3:
		return Data.Prevention;
	case 4:
		return Data.Treatment;
	case 5:
		return Data.Seedoc;
	}
	return "";
}
	
	
	
public class push extends AsyncTask<Void, Integer, Void>  {
	
	private View view;
	private ProgressDialog dialog;
	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		pushData();
		return null;
	}



	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		
		
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onPostExecute(Void arg) {
		// TODO Auto-generated method stub
		super.onPostExecute(arg);
		//Toast.makeText(getActivity().getApplicationContext(), Data.classifiedResult, Toast.LENGTH_LONG).show();
		//Intent i = new Intent(getActivity(),Results.class);
		//screenFragment.this.startActivity(i);
		//EngineVars.caller.onDestroy();
		Toast.makeText(getActivity().getApplicationContext(), "Thank you for the data", Toast.LENGTH_LONG).show();
		
		
	}
	
	private String pushData(){
		String introUrl = "http://myguide.pythonanywhere.com/add/";
		String dat = "(";
		for(int i=0;i<Data.selected.size()-1;i++){
			dat += String.valueOf(Data.selected.get(i))+",";
		}
		dat += String.valueOf(Data.selected.get(Data.selected.size()-1))+"/"+String.valueOf(Data.selectedDisease)+")/";
		
		Log.e("see this", dat);
		
		introUrl += dat;
		
		try{
			
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet req = new HttpGet(introUrl);
			HttpResponse response = httpclient.execute(req);
			Data.Intro = convertInputStreamToString(response.getEntity().getContent());
			
		}catch(Exception e){
			Log.e("error","err",e);
			return new String("err");
		}
		
		return new String("");
		
	}

	
	
	private  String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
 
        inputStream.close();
        return result;
 
    }
	
}

}