package com.code.Doc.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.code.Doc.Details;
import com.code.Doc.R;
import com.code.Doc.engine.Data;


public class busyFragment extends Fragment{
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 View vw = inflater.inflate(R.layout.fragment_busy, container, false);
		 Context context = getActivity().getApplicationContext();
		 ProgressBar pb = (ProgressBar)vw.findViewById(R.id.busy);
		 eng e = new eng();
		 e.execute(String.valueOf(Data.selectedDisease));
		 
		 return vw;
	}
	
	
public class eng extends AsyncTask<String, Integer, Void>  {
		
		private View view;
		private ProgressDialog dialog;
		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			
		}

		

		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
			String data = ((String[])params)[0];
			getall(data);
			return null;
		}



		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			
			
		}

		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		protected void onPostExecute(Void arg) {
			// TODO Auto-generated method stub
			super.onPostExecute(arg);
			//Toast.makeText(getActivity().getApplicationContext(), Data.classifiedResult, Toast.LENGTH_LONG).show();
			busyFragment.this.getActivity().finish();
			Intent i = new Intent(getActivity(),Details.class);
			busyFragment.this.startActivity(i);
			//EngineVars.caller.onDestroy();
			//Toast.makeText(getActivity().getApplicationContext(), Data.Symptoms, Toast.LENGTH_LONG).show();
			
		}
		
		private String getall(String id){
			String introUrl = "http://myguide.pythonanywhere.com/intro/("+id+")/";
			String causeUrl = "http://myguide.pythonanywhere.com/cause/("+id+")/";
			String symptomsUrl = "http://myguide.pythonanywhere.com/symptoms/("+id+")/";
			String preventionUrl = "http://myguide.pythonanywhere.com/prevention/("+id+")/";
			String treatmentUrl = "http://myguide.pythonanywhere.com/treatment/("+id+")/";
			String seeDocUrl = "http://myguide.pythonanywhere.com/seedoc/("+id+")/";
			
			try{
				
				HttpClient httpclient = new DefaultHttpClient();
				HttpGet req = new HttpGet(introUrl);
				HttpResponse response = httpclient.execute(req);
				Data.Intro = convertInputStreamToString(response.getEntity().getContent());
				
				req.setURI(new URI(causeUrl));
				response = httpclient.execute(req);
				Data.Cause = convertInputStreamToString(response.getEntity().getContent());
				
				req.setURI(new URI(symptomsUrl));
				response = httpclient.execute(req);
				Data.Symptoms = convertInputStreamToString(response.getEntity().getContent());
				
				req.setURI(new URI(preventionUrl));
				response = httpclient.execute(req);
				Data.Prevention = convertInputStreamToString(response.getEntity().getContent());
				
				req.setURI(new URI(treatmentUrl));
				response = httpclient.execute(req);
				Data.Treatment = convertInputStreamToString(response.getEntity().getContent());
				
				req.setURI(new URI(seeDocUrl));
				response = httpclient.execute(req);
				Data.Seedoc = convertInputStreamToString(response.getEntity().getContent());
				
			}catch(Exception e){
				Log.e("error","err",e);
				return new String("err");
			}
			
			return new String("");
			
		}

		
		
		private  String convertInputStreamToString(InputStream inputStream) throws IOException{
	        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	        String line = "";
	        String result = "";
	        while((line = bufferedReader.readLine()) != null)
	            result += line;
	 
	        inputStream.close();
	        return result;
	 
	    }
		
	}

}