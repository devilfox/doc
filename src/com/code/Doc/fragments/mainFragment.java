package com.code.Doc.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.code.Doc.R;
import com.code.Doc.engine.Engine;
import com.code.Doc.engine.EngineVars;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class mainFragment extends Fragment{
	View view = null;
	Context context = null;
	private ProgressBar cloudProgress = null;
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		this.getActivity().finish();;
		getFragmentManager().beginTransaction().remove(this).commit();
		super.onDestroy();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 view =  inflater.inflate(R.layout.fragment_main, container, false);
		 context = getActivity().getApplicationContext();
		 
		
		 
		  cloudProgress = (ProgressBar)view.findViewById(R.id.ourprogress);
		  if(cloudProgress != null)
			  cloudProgress.setMax(10);
		  
		 EngineVars.caller = this;
		EngineVars.view = view;
		 EngineVars.id = new Integer(0);
		EngineVars.progbar = cloudProgress;
		  
		Engine e = new Engine();
		e.execute();
		
//		 Intent i = new Intent(getActivity(),Screen.class);
//		 startActivity(i);
		 return view;
	}
	
	
public class check extends AsyncTask<Void, Integer, Boolean>  {
		
		private View view;
		private ProgressDialog dialog;
		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			
		}

		

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return isConnected(getActivity().getApplicationContext());
		}



		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			
			
		}

		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		protected void onPostExecute(Boolean arg) {
			// TODO Auto-generated method stub
			super.onPostExecute(arg);
			//Toast.makeText(getActivity().getApplicationContext(), Data.classifiedResult, Toast.LENGTH_LONG).show();
			
			if(!arg){
				Toast.makeText(getActivity().getApplicationContext(), "not connected to internet", Toast.LENGTH_LONG).show();
				getActivity().finish();
			}

			
//			Intent i = new Intent(getActivity(),Details.class);
//			screenFragment.this.startActivity(i);
			//EngineVars.caller.onDestroy();
		}
		
		public boolean isConnected(Context context){
	        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
	            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	            if (networkInfo != null && networkInfo.isConnected())
	                return true;
	            else
	                return false;  
	    }
	}

	
	
}