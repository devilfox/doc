package com.code.Doc.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

public class Data{
	public static String[] currentDiseaseList;
	public static Map<String,Integer> symptomList = new HashMap<String,Integer>();
	public static Map<String,String[]> organisedList = new HashMap<String, String[]>();
	public static String[] rank;
	public static String classifiedResult;
	
	public static List<Integer> selected = new ArrayList<Integer>();
	
	public static String Intro;
	public static String Cause;
	public static String Symptoms;
	public static String Prevention;
	public static String Treatment;
	public static String Seedoc;
	public static int selectedDisease;
	public static int currentpage ;
	
	
	public static int flag;
	
	public static String[] getCategories(){
		String[] categories = new String[Data.organisedList.keySet().size()];
		int i =0;
		for(String keys: Data.organisedList.keySet()){
			categories[i++] = keys;
		}
		return categories;
	}
	
	public static int getSelectedSymptomID(String symptom){
		return Data.symptomList.get(symptom)+1;
	}
	
	public static void clear(){
		currentDiseaseList = null;
		symptomList.clear();
		rank =null;
		classifiedResult = null;
		organisedList.clear();
		selected.clear();
	}
	
	public static void deleteValIfExists(int value){
		for(int i=0;i<selected.size();i++){
			if(selected.get(i) == value){
				selected.remove(i);
				return;
			}
		}
	}
	
	public static int getDiseaseIndex(String disease){
		for(int i=0;i<currentDiseaseList.length;i++){
			if(currentDiseaseList[i].trim().toUpperCase().equalsIgnoreCase(disease.trim().toUpperCase())){
				return i+1;
			}
		}
		return -1;
	}
}

