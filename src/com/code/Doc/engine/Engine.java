package com.code.Doc.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.code.Doc.Screen;
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Engine extends AsyncTask<Void, Integer, Integer>  {
	
	private View view;

	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		int value = ((Integer[])values)[0];
		
		EngineVars.progbar.setProgress(value);
		super.onProgressUpdate(values);
	}

	@Override
	protected Integer doInBackground(Void... params) {
		
		if(!isConnected(EngineVars.caller.getActivity().getApplicationContext())){
			return 0;
		}
		// TODO Auto-generated method stub
		view = EngineVars.view;
		int i= 0;
		Data.flag = new Integer(0);
		switch(EngineVars.id){
			case 0:
				Log.e("see", "1");
				publishProgress(2);
				i+=getSymptomsList();
				Log.e("see", "2");
				publishProgress(5);
				i+=getOrganisedList();
				Log.e("see", "3");
				publishProgress(8);
				i+= getCurrentDiseaseList();
				publishProgress(9);
				break;
			case 1:
				break;
			case 2:
				break;
		}
		Data.flag = i;
		String result = "done";
		return new Integer(i);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onPostExecute(Integer arg) {
		// TODO Auto-generated method stub
		super.onPostExecute(arg);
		if(arg != 0){
			Intent i = new Intent(EngineVars.caller.getActivity(),Screen.class);
			EngineVars.caller.startActivity(i);
			EngineVars.caller.onDestroy();
		}else{
			Toast.makeText(EngineVars.caller.getActivity().getApplicationContext(), "Please connect to internet", Toast.LENGTH_LONG).show();
		}
	
	}

	public String test(){
		String engineUrl = "http://myguide.pythonanywhere.com/getlist/";
		InputStream is = null;
		String result = "";
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet req = new HttpGet(engineUrl);
			HttpResponse response = httpclient.execute(req);
			is = response.getEntity().getContent();
			result = convertInputStreamToString(is);
		}catch(Exception e){
			
			Log.e("trace", "err",e);
			result = "exception";
		}
		return result;
	}
	
	private int getSymptomsList(){
		String engineUrl = "http://myguide.pythonanywhere.com/getlist/";
		InputStream is = null;
		String result = "";
		int res = 0;
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet req = new HttpGet(engineUrl);
			HttpResponse response = httpclient.execute(req);
			is = response.getEntity().getContent();
			result = convertInputStreamToString(is);
			result = result.substring(1,result.length()-1);
			String[] data = result.split(",");
			for(int i=0;i<data.length;i++){
				Data.symptomList.put(data[i],i);
			}
			result = String.valueOf(Data.symptomList.size());
			res = 1;
		}catch(Exception e){
			
			Log.e("trace", "err",e);
			result = "exception";
			return 0;
		}
		return res;
	}
	
	private int getOrganisedList(){
		String engineUrl = "http://myguide.pythonanywhere.com/classifiedlist/";
		InputStream is = null;
		String result = "";
		int res = 0;
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet req = new HttpGet(engineUrl);
			HttpResponse response = httpclient.execute(req);
			is = response.getEntity().getContent();
			result = convertInputStreamToString(is);
			result = result.substring(1,result.length()-1);
			String[] broken = result.split("&");
			for(String each:broken){
				String[] fine = each.split(">"); //[0]:symptoms [1]:group
				Data.organisedList.put(fine[fine.length-1].trim().toUpperCase(), fine[0].split(","));
			}
			result = String.valueOf(Data.organisedList.size());
			res = 2;
			
		}catch(Exception e){
			
			Log.e("trace", "err",e);
			result = "exception";
			return 0;
		}
		return res;
	}
	
	private int getCurrentDiseaseList(){
		String engineUrl = "http://myguide.pythonanywhere.com/dlist/";
		InputStream is = null;
		int res = 0;
		String result = "";
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet req = new HttpGet(engineUrl);
			HttpResponse response = httpclient.execute(req);
			is = response.getEntity().getContent();
			result = convertInputStreamToString(is);
			result = result.substring(1,result.length()-1);
			Data.currentDiseaseList = result.split(",");
			result = String.valueOf(Data.currentDiseaseList.length);
			res = 3;
			
		}catch(Exception e){
			
			Log.e("trace", "err",e);
			result = "exception";
			return 0;
		}
		return res;
	}
	
	private String classifyData(String data){
		String engineUrl = "http://myguide.pythonanywhere.com/classify/"+data+'/';
		InputStream is = null;
		String result = "";
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet req = new HttpGet(engineUrl);
			HttpResponse response = httpclient.execute(req);
			is = response.getEntity().getContent();
			result = convertInputStreamToString(is);
			Data.classifiedResult = result;
		}catch(Exception e){
			
			Log.e("trace", "err",e);
			result = "exception";
		}
		return result;
	}
	
	private String rankData(String data){
		String engineUrl = "http://myguide.pythonanywhere.com/rank/"+data+'/';
		InputStream is = null;
		String result = "";
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet req = new HttpGet(engineUrl);
			HttpResponse response = httpclient.execute(req);
			is = response.getEntity().getContent();
			result = convertInputStreamToString(is);
			result = result.substring(1,result.length()-1);
			Data.rank = result.split("&");
		}catch(Exception e){
			
			Log.e("trace", "err",e);
			result = "exception";
		}
		return result;
	}
	
	private  String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
 
        inputStream.close();
        return result;
 
    }
	
	public boolean isConnected(Context context){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected())
                return true;
            else
                return false;  
    }
}
